import PropTypes from 'prop-types'

function Button(props) {
    const onClick = (e) => {
        alert('Not Working!\nPlease wait till next update...');
    }
    
    const buttonStyle = {
        backgroundColor: props.color
    }
    return (
        <button 
            onClick={onClick}
            className='btn' style={buttonStyle}>
            {props.text}
        </button>
    )
}

Button.defaultProps = {
    color: "blue",
    text: "Click Me"
}

Button.propTypes = {
    text: PropTypes.string,
    color: PropTypes.string
}


export default Button
