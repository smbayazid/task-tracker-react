// import React from 'react'
import Button from './Button'

import PropTypes from 'prop-types'
const Header = (props) => {
    return (
        <header className="header">
            <h1>
                {props.title}
            </h1>
            <Button color='green' text='Add Event'/>
            {/* <Button />
            <Button babe='red' text='Delete Event'/>
            <Button babe='black' text='Love You'/> */}
        </header>
    )
}

// Header.defaultProps = {
//     title: 'Task Tracker'
// }

// Header.propTypes = {
//     title: PropTypes.string.isRequired,
// }

export default Header
