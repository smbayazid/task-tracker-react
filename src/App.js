// import React from 'react'
import Header from './components/Header'
import Tasks from './components/Tasks'
import { useState } from 'react'
import AddTask from './components/AddTask'

function App() {
  // const name = 'Sufi'
  // const flag = true
  const [tasks, setTasks] = useState([
    {
      id: 1,
      title: "Doctor's Appoinment",
      day: "27/03/2021 at 03:45 pm",
      reminder: true,
    },
    {
      id: 2,
      title: "Math Class",
      day: "26/03/2021 at 12:30 pm",
      reminder: false,
    },
    {
      id: 3,
      title: "Meeting with Jinnah",
      day: "01/04/2021 at 09:00 pm",
      reminder: true,
    },
    {
      id: 4,
      title: "EPL Match - II",
      day: "06/04/2021 at 05:00 pm",
      reminder: true,
    }
  ])

  // Delete Task
  const deleteTask = (id) => {
    // alert("Delete:" + id);
    setTasks(tasks.filter((task) => task.id !== id));
  } 

  // Toggle Reminder
  const toggleReminder = (id) => {
    // console.log("Toggling", id);
    setTasks(
      tasks.map((task) => 
        task.id === id ?
        {...task, reminder: !task.reminder} : task
      )
    )
  }

  return (
    <div className="container">
      {/* <h1>Task Tracker</h1> */}
      {/* <h3>Hello {name}</h3> */}
      {/* <p>{flag ? 'You are registered.' : 'You are stranger!!!'}</p> */}
      <Header title = "Task Tracker"/>
      <AddTask />
      {tasks.length > 0 ?
        <Tasks 
          tasks = {tasks}
          onDelete = {deleteTask}
          onToggle = {toggleReminder}
        /> : 'No Tasks To Show...'}
    </div>
  );
}

// class App extends React.Component{
//   render(){
//     return(
//       <div>
//         <h3>Hello From Class!!</h3>
//       </div>
//     );
//   }
// }

export default App;
